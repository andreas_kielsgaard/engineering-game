﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {

	public static LevelManager currentManager;
	public List<Goal> goals = new List<Goal>();
	public List<Rocket> players = new List<Rocket>();

	// Use this for initialization
	void Start () {
		currentManager = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void ResetLevel() {
		foreach(Rocket p in players) {
			p.Reset();
		}
		foreach(Goal g in goals) {
			g.Reset();
		}
	}
}
