﻿using UnityEngine;
using System.Collections;

public class StatManager : MonoBehaviour {

	[SerializeField]
	float credits = 0;
	public static StatManager currentStatManager;

	// Use this for initialization
	void Start () {
		currentStatManager = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Award(float reward) {
		credits += reward;
	}
}
