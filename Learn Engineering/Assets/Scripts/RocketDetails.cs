﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class RocketDetails{

	public float impulsePower;
	public float continuousPower;
	public float continuousTime;

}
