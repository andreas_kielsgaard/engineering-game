﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour {

	public Rigidbody rb;
	bool hasLaunched = false;
	public RocketDetails rocketPhysics;
	float launchTime;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
			if(hasLaunched == false) LaunchPlayer();
			hasLaunched = true;
		}
		if(Input.GetKeyDown(KeyCode.Escape)) {
			Reset();
		}
		if(rocketPhysics.continuousTime - Time.time > 0) {
			rb.AddForce(rocketPhysics.continuousPower * Vector3.up);
		}
	}

	public StatManager getStats() {
		return StatManager.currentStatManager;
	}

	void LaunchPlayer() {
		launchTime = Time.time;
		rb.AddForce(rocketPhysics.impulsePower * Vector3.up, ForceMode.Impulse);
	}

	public void Reset() {
		transform.position = new Vector3(0, 0, 0);
		rb.velocity = Vector3.zero;
		rb.angularVelocity = Vector3.zero;
		hasLaunched = false;
    }

	void OnCollisionEnter(Collision c) {
		Debug.Log("Touchdown on " + (Time.time - launchTime));
		if(c.gameObject.name == "Terrain") {
			Reset();
		}
	}


}
