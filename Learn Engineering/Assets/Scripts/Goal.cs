﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {

	public float creditsAwarded = 10;
	public bool hasBeenAwarded = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider c) {
		Rocket r = c.gameObject.GetComponent<Rocket>();
        if(r != null && !hasBeenAwarded){
			hasBeenAwarded = true;
			r.getStats().Award(creditsAwarded);
		}
	}

	public void Reset() {
		hasBeenAwarded = false;
	}
}
